<?php
/*

  TODO , Type Cast SimpleXML Objects To Strings And Integers


*/
require_once('GDB_Game.php');
require_once('GDB_Platform.php');

class GDB_Request{

    public function __construct($printProgress=false) {
           $this->baseURI       = 'http://thegamesdb.net/api/';
           $this->baseImagesURI = 'http://thegamesdb.net/banners/';

           if(defined('STDIN') )
              $this->terminal = true;
            else
              $this->terminal = false;

            $this->printProgress = $printProgress;
    }

    public function  getPlatforms($minimumInfo=false){
        $platformsURI = $this->baseURI . 'GetPlatformsList.php';
        $platformURI  = $this->baseURI . 'GetPlatform.php';


        // If minimum
        if($minimumInfo){

          # Get XML File
          $ch = curl_init($platformsURI);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          $xmlSTRResponse = curl_exec($ch);
          curl_close($ch);

          if(!$xmlSTRResponse)
              return Null;

          $platformsXML = new SimpleXMLElement($xmlSTRResponse);

          if(isset($platformsXML->Platforms->Platform)){

            $platforms = [];
            foreach($platformsXML->Platforms->Platform as $platformXMLObj){

                $platform = new GDB_Platform();

                // Set Details To Object Platform
                $platform->setID(      isset($platformXMLObj->id)       ? (integer)$platformXMLObj->id       : Null);
                $platform->setPlatform(isset($platformXMLObj->name)     ? (string) $platformXMLObj->name : Null);

                $platforms[] = $platform;
            }

            return $platforms;
          }
          else
            return NUll;
        }



        $platformsIDs = $this->getPlatformsIDs();
        if(is_null($platformsIDs))
            return Null;


        if($this->printProgress) {

            $this->flushMessage('Got ' . count($platformsIDs) . ' Platforms IDs. ');
            $this->flushMessage('Retrieving Platforms ');
        }

        $platforms = [];
        $downloadedCounter = 1;
        foreach($platformsIDs as $platformID){
            $platform = $this->getPlatformByID($platformID);
            if(is_null($platform))
              return Null;
            else
              $platforms[] = $platform;

            if($this->printProgress) {
              $this->flushMessage('Downloaded ' . $downloadedCounter  . ' out of ' . count($platformsIDs) . ' Platforms ');
              $downloadedCounter += 1;
            }
        }

        if(count($platforms) > 0 )
          return $platforms;
        else
          return Null;
    }

    public function getGamesByPlatformID($platformID,$minimumInfo=false){
        $platformGamesURI = $this->baseURI . 'GetPlatformGames.php?platform=' . $platformID;

        # Get XML File
        $ch = curl_init($platformGamesURI);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $xmlSTRResponse = curl_exec($ch);
        curl_close($ch);

        if(!$xmlSTRResponse)
            return Null;

        $gamesXML = new SimpleXMLElement($xmlSTRResponse);

        // If we only want id and title
        if($minimumInfo){

          $games = [];
          if(isset($gamesXML->Game)) {

            foreach ($gamesXML->Game as $game){

              $gameObj    = new GDB_Game();


              $gameObj->setID(         isset($game->id)                ? (integer)$game->id               : Null);
              $gameObj->setGameTitle(  isset($game->GameTitle)         ? (string) $game->GameTitle         : Null);

              $games[] = $gameObj;
            }
          }
          else
            return Null;

          if(count($games) == 0)
            return Null;
          else
            return $games;
        }

        // If we want full game details get ids and request info for every game
        $gamesIDs = [];
        if(isset($gamesXML->Game)) {

        	foreach ($gamesXML->Game as $game){
        		$gamesIDs[] = (integer)$game->id;
        	}
        }
        else
        	return Null;

       if(count($gamesIDs) == 0)
       	return Null;

       if($this->printProgress) {
       		$this->flushMessage('Got ' . count($gamesIDs) . ' Game IDs.');
       		$this->flushMessage('Retrieving Games');
       }

       $games = [];
       $downloadedCounter = 1;
       foreach($gamesIDs as $gameID){
       		$game = $this->getGameByID($gameID);
       		if(is_null($game))
       			return Null;
       		else
       			$games[] = $game;

       		if($this->printProgress) {
       			$this->flushMessage('Downloaded ' . $downloadedCounter  . ' out of ' . count($gamesIDs) . ' Games');
       			$downloadedCounter += 1;
       		}
       }


       return $games;
    }

    public function getPlatformByID($platformID){
      $platformURI  = $this->baseURI . 'GetPlatform.php?id=' . $platformID;


      # Get XML File
      $ch = curl_init($platformURI);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $xmlSTRResponse = curl_exec($ch);
      curl_close($ch);

      if(!$xmlSTRResponse)
          return Null;

      $platformXML = new SimpleXMLElement($xmlSTRResponse);
      if(isset($platformXML->Platform)) {

        $platform       = new GDB_Platform();
        $platformXMLObj = $platformXML->Platform;

        // Set Details To Object Platform
        $platform->setID(            isset($platformXMLObj->id)             ? (integer)$platformXMLObj->id            : Null);
        $platform->setPlatform(      isset($platformXMLObj->Platform)       ? (string)$platformXMLObj->Platform       : Null);
        $platform->setOverview(      isset($platformXMLObj->overview)       ? (string)$platformXMLObj->overview       : Null);
        $platform->setDeveloper(     isset($platformXMLObj->developer)      ? (string)$platformXMLObj->developer      : Null);
        $platform->setManufacturer(  isset($platformXMLObj->manufacturer)   ? (string)$platformXMLObj->manufacturer   : Null);
        $platform->setCpu(           isset($platformXMLObj->cpu)            ? (string)$platformXMLObj->cpu            : Null);
        $platform->setMemory(        isset($platformXMLObj->memory)         ? (string)$platformXMLObj->memory         : Null);
        $platform->setGraphics(      isset($platformXMLObj->graphics)       ? (string)$platformXMLObj->graphics       : Null);
        $platform->setSound(         isset($platformXMLObj->sound)          ? (string)$platformXMLObj->sound          : Null);
        $platform->setDisplay(       isset($platformXMLObj->display)        ? (string)$platformXMLObj->display        : Null);
        $platform->setMedia(         isset($platformXMLObj->media)          ? (string)$platformXMLObj->media          : Null);
        $platform->setMaxControllers(isset($platformXMLObj->maxcontrollers) ? (string)$platformXMLObj->maxcontrollers : Null);
        $platform->setRating(        isset($platformXMLObj->Rating)         ? (float)$platformXMLObj->Rating          : Null);
        // Note* maxControllers is string because one of the possible values returned is keyboard

        // Commented Out Because API currently returns an invalid youtube link
        //$platform->setConsole(isset($platformXMLObj->console) ? $platformXMLObj->console : Null);
        //$platform->setController(isset($platformXMLObj->controller)  ? $platformXMLObj->controller : Null);

        // Set Box Arts
        if(isset($platformXMLObj->Images->boxart)){
            $boxArts = [];

            foreach($platformXMLObj->Images->boxart as $boxArt){

                #Todo - More than one boxarts with same side may be available
                if($boxArt->attributes()['side'] == 'back')
                  $boxArts['back']  = $this->baseImagesURI . (string)$boxArt;
                elseif($boxArt->attributes()['side'] == 'front')
                  $boxArts['front']  = $this->baseImagesURI .(string)$boxArt;
            }

            $platform->setBoxArts( count($boxArts) > 0 ? $boxArts : Null);
        }


        // Set Fan Arts
        if(isset($platformXMLObj->Images->fanart)){
            $fanArts = [];
            foreach($platformXMLObj->Images->fanart as $value) {

                $fanArts[] = $this->baseImagesURI . (string)$value->original;

            }

            $platform->setFanArts( count($fanArts) > 0 ?  $fanArts : Null);
        }

        return $platform;
      }
      else
        return Null;

    }

    /**
     * Info will be added TODO
     *
     *
     */
    public function getGameByID($gameID){
        $gameURI = $this->baseURI . 'GetGame.php';
        $requestURI = $gameURI . "?id={$gameID}";

        # Get XML File
        $ch = curl_init($requestURI);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $xmlSTRResponse = curl_exec($ch);
        curl_close($ch);

        if(!$xmlSTRResponse)
            return Null;

          $gameXML = Null;

          try {
            $gameXML = new SimpleXMLElement($xmlSTRResponse);
          } catch (Exception $e) {
              error_log("Couldn't parse XML for Game With ID : {$gameID}");
              return Null;
          }

        $game    = new GDB_Game();


        $game->setID(         isset($gameXML->Game->id)                ? (integer)$gameXML->Game->id               : Null);
        $game->setGameTitle(  isset($gameXML->Game->GameTitle)         ? (string)$gameXML->Game->GameTitle         : Null);
        $game->setPlatform(   isset($gameXML->Game->Platform)          ? (string)$gameXML->Game->Platform          : Null);
        $game->setPlatformID( isset($gameXML->Game->PlatformId)        ? (integer)$gameXML->Game->PlatformId       : Null);
        $game->setReleaseDate(isset($gameXML->Game->ReleaseDate)       ? (string)$gameXML->Game->ReleaseDate       : Null);
        $game->setOverview(   isset($gameXML->Game->Overview)          ? (string)$gameXML->Game->Overview          : Null);
        $game->setESRB(       isset($gameXML->Game->ESRB)              ? (string)$gameXML->Game->ESRB              : Null);
        $game->setGenres(     isset($gameXML->Game->Genres->genre)     ? (array)$gameXML->Game->Genres->genre      : Null);
        $game->setYoutube(    isset($gameXML->Game->Youtube)           ? (string)$gameXML->Game->Youtube           : Null);
        $game->setPublisher(  isset($gameXML->Game->Publisher)         ? (string)$gameXML->Game->Publisher         : Null);
        $game->setDeveloper(  isset($gameXML->Game->Developer)         ? (string)$gameXML->Game->Developer         : Null);
        $game->setRating(     isset($gameXML->Game->Rating)            ? (float)$gameXML->Game->Rating             : Null);
        $game->setClearLogo(  isset($gameXML->Game->Images->clearlogo) ? (string)$gameXML->Game->Images->clearlogo : Null);

        // We should remove attributes from banner and add base uri ! TODO
        $game->setBanner(     isset($gameXML->Game->Images->banner)    ? (array)$gameXML->Game->Images->banner     : Null);

        // Set Box Arts
        if(isset($gameXML->Game->Images->boxart)){
            $boxArts = [];
            foreach((array)$gameXML->Game->Images->boxart as $boxArt){

                if(is_string($boxArt)){
                    if(strpos($boxArt,'front'))
                        $boxArts['front'] = $this->baseImagesURI . (string)$boxArt;
                    elseif(strpos($boxArt,'back'))
                        $boxArts['back'] = $this->baseImagesURI . (string)$boxArt;
                }
            }
            $game->setBoxArts( count($boxArts) > 0 ? $boxArts : Null);
        }


        // Set Fan Arts
        if(isset($gameXML->Game->Images->fanart)){
            $fanArts = [];
            foreach($gameXML->Game->Images->fanart as $value) {

                $fanArts[] = $this->baseImagesURI . (string)$value->original;

            }

            $game->setFanArts( count($fanArts) > 0 ?  $fanArts : Null);
        }


        // Set Screenshots
        if(isset($gameXML->Game->Images->screenshot)) {
            $screenshots = [];
            foreach($gameXML->Game->Images->screenshot as $value) {

                $screenshots[] = $this->baseImagesURI . (string)$value->original;

            }

            $game->setScreenshot( count($screenshots) > 0 ?  $screenshots : Null);
        }

        return $game;
    }

    public function countPlatforms(){

      $platformsIDs = $this->getPlatformsIDs();
      if(is_null($platformsIDs))
        return Null;
      else
        return count($platformsIDs);
    }

    public function countGames(){

      $platformsIDs = $this->getPlatformsIDs();
      if(is_null($platformsIDs))
        return Null;

      $totalGames = 0;

      $downloadedCounter = 0;
      foreach($platformsIDs as $platformID){

        $totalGames +=  $this->countGamesOfPlatform($platformID);

        $downloadedCounter +=1;

        if($this->printProgress) {
          $totalPlatforms = count($platformsIDs);
          $this->flushMessage("Total Games: {$totalGames} , Platform {$downloadedCounter} out of {$totalPlatforms}");
        }

      }

      return $totalGames;


    }

    public function countGamesOfPlatform($platformID){

      $platformGamesURI =  $this->baseURI . 'GetPlatformGames.php?platform=' .$platformID;

      # Get XML File
      $ch = curl_init($platformGamesURI);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $xmlSTRResponse = curl_exec($ch);
      curl_close($ch);

      if(!$xmlSTRResponse)
          return Null;

      $gamesXML = new SimpleXMLElement($xmlSTRResponse);
      if(isset($gamesXML->Game))
        return count($gamesXML->Game);
      else
        return 0; // Platform may not have any games, we can't really check that
    }

    private function getPlatformsIDs(){
      $platformsURI = $this->baseURI . 'GetPlatformsList.php';

      # Get XML File
      $ch = curl_init($platformsURI);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $xmlSTRResponse = curl_exec($ch);
      curl_close($ch);

      if(!$xmlSTRResponse)
          return Null;

      $platformsXML = new SimpleXMLElement($xmlSTRResponse);

      $platformsIDs = [];
      if(isset($platformsXML->Platforms->Platform)){
        foreach($platformsXML->Platforms->Platform as $platform){
            $platformsIDs[] = (integer)$platform->id;
        }
        return $platformsIDs;
      }
      else
        return Null;
    }

    private function flushMessage($message){

      $newLine =  $this->terminal ? "\n" : "<br>";

      echo $message . $newLine;
      flush();

      if( ob_get_level() > 0 ) ob_flush();
    }

    private $baseURI;
    private $baseImagesURI;
    private $terminal;
    private $flushProgress;
}

?>