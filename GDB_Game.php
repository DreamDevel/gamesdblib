<?php 

class GDB_Game {

    public function __construct() {
        $this->imagesURI = 'http://thegamesdb.net/banners/';
    }

    public function getID()          {return $this->id;         }
    public function getGameTitle()   {return $this->gameTitle;  }
    public function getPlatform()    {return $this->platform;   }
    public function getPlatformID()  {return $this->platformID; }
    public function getReleaseDate() {return $this->releaseDate;}
    public function getOverview()    {return $this->overview;   }
    public function getESRB()        {return $this->esrb;       }
    public function getGenres()      {return $this->genres;     }
    public function getYoutube()     {return $this->youtube;    }
    public function getPublisher()   {return $this->publisher;  }
    public function getDeveloper()   {return $this->developer;  }
    public function getRating()      {return $this->rating;     }
    public function getFanArts()     {return $this->fanArts;    }
    public function getBoxArts()     {return $this->boxArts;    }
    public function getBanner()      {return $this->banner;     }
    public function getScreenshot()  {return $this->screenshot; }
    public function getClearLogo()   {return $this->clearLogo;  }
    public function getImagesURI()   {return $this->imagesURI;  }

    public function setID($id)                   {$this->id          = $id;         }
    public function setGameTitle($gameTitle)     {$this->gameTitle   = $gameTitle;  }
    public function setPlatform($platform)       {$this->platform    = $platform;   }
    public function setPlatformID($platformID)   {$this->platformID  = $platformID; }
    public function setReleaseDate($releaseDate) {$this->releaseDate = $releaseDate;}
    public function setOverview($overview)       {$this->overview    = $overview;   }
    public function setESRB($esrb)               {$this->esrb        = $esrb;       }
    public function setGenres($genres)           {$this->genres      = $genres;     }
    public function setYoutube($youtube)         {$this->youtube     = $youtube;    }
    public function setPublisher($publisher)     {$this->publisher   = $publisher;  }
    public function setDeveloper($developer)     {$this->developer   = $developer;  }
    public function setRating($rating)           {$this->rating      = $rating;     }
    public function setFanArts($fanArts)         {$this->fanArts     = $fanArts;    }
    public function setBoxArts($boxArts)         {$this->boxArts     = $boxArts;    }
    public function setBanner($banner)           {$this->banner      = $banner;     }
    public function setScreenshot($screenshot)   {$this->screenshot  = $screenshot; }
    public function setClearLogo($clearLogo)     {$this->clearLogo   = $clearLogo;  }

    public function __toString()
    {

        $unavailableStr = 'Unknown';

        $newLine = defined('STDIN') ? "\n" : '<br>';

        $gameStr  = 'Id          : ' . $this->id           . $newLine;
        
        if ( !is_null($this->gameTitle) )  
            $gameStr .= 'Game Title  : ' . $this->gameTitle    . $newLine;
        else
            $gameStr .= 'Game Title  : ' . $unavailableStr     . $newLine;

        if( !is_null($this->platform) ) 
            $gameStr .= 'Platform    : ' . $this->platform     . $newLine;
        else
            $gameStr .= 'Platform    : ' . $unavailableStr     . $newLine;

        if( !is_null($this->platformID) ) 
            $gameStr .= 'Platform ID : ' . $this->platformID   . $newLine;
        else
            $gameStr .= 'Platform ID : ' . $unavailableStr     . $newLine;

        if( !is_null($this->releaseDate) ) 
            $gameStr .= 'Release Date: ' . $this->releaseDate  . $newLine;
        else
            $gameStr .= 'Release Date: ' . $unavailableStr     . $newLine;

        if( !is_null($this->overview) ) 
            $gameStr .= 'Overview    : ' . $this->overview     . $newLine;
        else
            $gameStr .= 'Overview    : ' . $unavailableStr     . $newLine;

        if( !is_null($this->esrb) ) 
            $gameStr .= 'ESRB        : ' . $this->esrb         . $newLine;
        else
            $gameStr .= 'ESRB        : ' . $unavailableStr     . $newLine;


        // Display Genres Array
        if( !is_null($this->genres) ) {
            
            if(is_array($this->genres)) {
                $gameStr .= 'Genres      : ';
                foreach($this->genres as $genre)
                    $gameStr .=  $genre . ',';
                $gameStr .= $newLine;
            }
            else
                $gameStr .= 'Genres      : ' . $this->genres   . $newLine;
        }
        else
            $gameStr .= 'Genres      : ' . $unavailableStr     . $newLine;


        if( !is_null($this->youtube) ) 
            $gameStr .= 'Youtube     : ' . $this->youtube      . $newLine;
        else
            $gameStr .= 'Youtube     : ' . $unavailableStr     . $newLine;

        if( !is_null($this->publisher) ) 
            $gameStr .= 'Publisher   : ' . $this->publisher    . $newLine;
        else
            $gameStr .= 'Publisher   : ' . $unavailableStr     . $newLine;

        if( !is_null($this->developer) ) 
            $gameStr .= 'Developer   : ' . $this->developer    . $newLine;
        else
            $gameStr .= 'Developer   : ' . $unavailableStr     . $newLine;

        if( !is_null($this->rating) ) 
            $gameStr .= 'Rating      : ' . $this->rating       . $newLine;
        else
            $gameStr .= 'Rating      : ' . $unavailableStr     . $newLine;


        // Display Fan Arts Array
        if( !is_null($this->fanArts) ) {

            if(is_array($this->fanArts)) {
                $gameStr .= 'Fan Arts    : ';
                foreach($this->fanArts as $fanArt)
                    $gameStr .=  $fanArt . ' , ';
                $gameStr .= $newLine;
            }
            else
                $gameStr .= 'Fan Art     : ' . $this->fanArts      . $newLine;
        }
        else 
            $gameStr .= 'Fan Art     : ' . $unavailableStr     . $newLine;


        // Display Box Arts Array
        if( !is_null($this->boxArts) ) {

            if(is_array($this->boxArts)) {
                $gameStr .= 'Box Arts    : ';
                foreach($this->boxArts as $key => $value)
                    $gameStr .=  $value . ' , ';
                $gameStr .= $newLine;
            }
            else
                $gameStr .= 'Box Arts    : ' . $this->boxArts      . $newLine;
        }
        else
            $gameStr .= 'Box Arts    : ' . $unavailableStr     . $newLine;


        if( !is_null($this->banner) ) 
            $gameStr .= 'Banner      : ' . $this->banner       . $newLine;
        else
            $gameStr .= 'Banner      : ' . $unavailableStr     . $newLine;


        // Display Screenshot Array
        if( !is_null($this->screenshot) ) {

            if(is_array($this->screenshot)) {
                $gameStr .= 'Screenshot  : ';
                foreach($this->screenshot as $screenshot)
                    $gameStr .=  $screenshot . ' , ';
                $gameStr .= $newLine;
            }
            else
                $gameStr .= 'Screenshot  : ' . $this->screenshot   . $newLine;
        }
        else 
            $gameStr .= 'Screenshot  : ' . $unavailableStr     . $newLine;


        if( !is_null($this->clearLogo) ) 
            $gameStr .= 'Clear Logo  : ' . $this->clearLogo    . $newLine;
        else 
            $gameStr .= 'Clear Logo  : ' . $unavailableStr     . $newLine;
        
        return $gameStr;
    }

    private $id;
    private $gameTitle;
    private $platform;
    private $platformID;
    private $releaseDate;
    private $overview;
    private $esrb;
    private $genres;
    private $youtube;
    private $publisher;
    private $developer;
    private $rating;
    private $fanArts;
    private $boxArts;
    private $banner;
    private $screenshot;
    private $clearLogo;
    private $imagesURI;

}

?>