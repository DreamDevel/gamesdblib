<?php 

class GDB_Platform {

    public function __construct() {
        $this->imagesURI = 'http://thegamesdb.net/banners/';
    }
    
    public function getID()            {return $this->id;            }
    public function getPlatform()      {return $this->platform;      }
    public function getConsole()       {return $this->console;       }
    public function getController()    {return $this->controller;    }
    public function getOverview()      {return $this->overview;      }
    public function getDeveloper()     {return $this->developer;     }
    public function getManufacturer()  {return $this->manufacturer;  }
    public function getCpu()           {return $this->cpu;           }
    public function getMemory()        {return $this->memory;        }
    public function getGraphics()      {return $this->graphics;      }
    public function getSound()         {return $this->sound;         }
    public function getDisplay()       {return $this->display;       }
    public function getMedia()         {return $this->media;         }
    public function getMaxControllers(){return $this->maxControllers;} 
    public function getRating()        {return $this->rating;        }
    public function getFanArts()       {return $this->fanArts;       }
    public function getBoxArts()       {return $this->boxArts;       }
    public function getConsoleArts()   {return $this->consoleArts;   }
    public function getControllerArts(){return $this->controllerArts;}

    public function setID($id)                          {$this->id             = $id;             }
    public function setPlatform($platform)              {$this->platform       = $platform;       }
    public function setConsole($console)                {$this->console        = $console;        }
    public function setController($controller)          {$this->controller     = $controller;     }
    public function setOverview($overview)              {$this->overview       = $overview;       }
    public function setDeveloper($developer)            {$this->developer      = $developer;      }
    public function setManufacturer($manufacturer)      {$this->manufacturer   = $manufacturer;   }
    public function setCpu($cpu)                        {$this->cpu            = $cpu;            }
    public function setMemory($memory)                  {$this->memory         = $memory;         }
    public function setGraphics($graphics)              {$this->graphics       = $graphics;       }
    public function setSound($sound)                    {$this->sound          = $sound;          }
    public function setDisplay($display)                {$this->display        = $display;        }
    public function setMedia($media)                    {$this->media          = $media;          }
    public function setMaxControllers($maxControllers)  {$this->maxControllers = $maxControllers; } 
    public function setRating($rating)                  {$this->rating         = $rating;         }
    public function setFanArts($fanArts)                {$this->fanArts        = $fanArts;        }
    public function setBoxArts($boxArts)                {$this->boxArts        = $boxArts;        }
    public function setConsoleArts($consoleArts)        {$this->consoleArts    = $consoleArts;    }
    public function setControllerArts($controllerArts)  {$this->controllerArts = $controllerArts; }

    
    public function __toString()
    {

        $unavailableStr = 'Unknown';

        $newLine = defined('STDIN') ? "\n" : '<br>';


        $gameStr  = 'Id          : ' . $this->id           . $newLine;
        
        if ( !is_null($this->platform) )  
            $gameStr .= 'Platform    : ' . $this->platform    . $newLine;
        else
            $gameStr .= 'Platform    : ' . $unavailableStr     . $newLine;

        if( !is_null($this->console) ) 
            $gameStr .= 'Console     : ' . $this->console    . $newLine;
        else
            $gameStr .= 'Console    : ' . $unavailableStr     . $newLine;

        if( !is_null($this->controller) ) 
            $gameStr .= 'Controller  : ' . $this->controller   . $newLine;
        else
            $gameStr .= 'Controller  : ' . $unavailableStr     . $newLine;

        if( !is_null($this->overview) ) 
            $gameStr .= 'Overview    : ' . $this->overview     . $newLine;
        else
            $gameStr .= 'Overview    : ' . $unavailableStr     . $newLine;

        if( !is_null($this->developer) ) 
            $gameStr .= 'Developer   : ' . $this->developer    . $newLine;
        else
            $gameStr .= 'Developer   : ' . $unavailableStr     . $newLine;


        if( !is_null($this->manufacturer) ) 
            $gameStr .= 'Manufacturer   : ' . $this->manufacturer   . $newLine;
        else
            $gameStr .= 'Manufacturer   : ' . $unavailableStr     . $newLine;

        if( !is_null($this->cpu) ) 
            $gameStr .= 'CPU         : ' . $this->cpu          . $newLine;
        else
            $gameStr .= 'CPU         : ' . $unavailableStr     . $newLine;


        if( !is_null($this->memory) ) 
            $gameStr .= 'Memory      : ' . $this->memory       . $newLine;
        else
            $gameStr .= 'Memory      : ' . $unavailableStr     . $newLine;

        if( !is_null($this->graphics) ) 
            $gameStr .= 'Graphics     : ' . $this->graphics       . $newLine;
        else
            $gameStr .= 'Graphics     : ' . $unavailableStr     . $newLine;

        if( !is_null($this->sound) ) 
            $gameStr .= 'Sound       : ' . $this->sound       . $newLine;
        else
            $gameStr .= 'Sound       : ' . $unavailableStr     . $newLine;

        if( !is_null($this->display) ) 
            $gameStr .= 'Display      : ' . $this->display       . $newLine;
        else
            $gameStr .= 'Display      : ' . $unavailableStr     . $newLine;

        if( !is_null($this->media) ) 
            $gameStr .= 'Media      : ' . $this->media       . $newLine;
        else
            $gameStr .= 'Media      : ' . $unavailableStr     . $newLine;

        if( !is_null($this->maxControllers) ) 
            $gameStr .= 'Max Controllers      : ' . $this->maxControllers       . $newLine;
        else
            $gameStr .= 'Max Controllers      : ' . $unavailableStr     . $newLine;

        if( !is_null($this->rating) ) 
            $gameStr .= 'Rating      : ' . $this->rating       . $newLine;
        else
            $gameStr .= 'Rating      : ' . $unavailableStr     . $newLine;

        // Display Fan Arts Array
        if( !is_null($this->fanArts) ) {

            if(is_array($this->fanArts)) {
                $gameStr .= 'Fan Arts    : ';
                foreach($this->fanArts as $fanArt)
                    $gameStr .=  $fanArt . ' , ';
                $gameStr .= $newLine;
            }
            else
                $gameStr .= 'Fan Art     : ' . $this->fanArts      . $newLine;
        }
        else 
            $gameStr .= 'Fan Art     : ' . $unavailableStr     . $newLine;


        // Display Box Arts Array
        if( !is_null($this->boxArts) ) {

            if(is_array($this->boxArts)) {
                $gameStr .= 'Box Arts    : ';
                foreach($this->boxArts as $key => $value)
                    $gameStr .=  $value . ' , ';
                $gameStr .= $newLine;
            }
            else
                $gameStr .= 'Box Arts    : ' . $this->boxArts      . $newLine;
        }
        else
            $gameStr .= 'Box Arts    : ' . $unavailableStr     . $newLine;


        // TODO Add consoleArts and controllerArts
        return $gameStr;
    }

    private $id;
    private $platform;
    private $console;
    private $controller;
    private $overview;
    private $developer;
    private $manufacturer;
    private $cpu;
    private $memory;
    private $graphics;
    private $sound;
    private $display;
    private $media;
    private $maxControllers;
    private $rating;
    private $fanArts;
    private $boxArts;
    private $consoleArts;
    private $controllerArts;

}

?>